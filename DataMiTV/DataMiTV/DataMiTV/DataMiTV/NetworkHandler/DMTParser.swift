//
//  DMTParser.swift
//  DataMiTV
//
//  Created by Sandeep Lall on 18/02/19.
//  Copyright © 2019 Sandeep Lall. All rights reserved.
//

import UIKit
import SwiftyJSON

class DMTParser: NSObject {

    func parseFeaturedPackageData(swiftyJsonVar: JSON)->Array<DMTPackageDetails> {
        
        var packageDetailsList:Array<DMTPackageDetails> =  Array()
        
        let json = JSON(swiftyJsonVar)
        
        for (key,packageJSON) in json {
            print("key-------->\(key)")
            print("packageDic-------->\(packageJSON["id"])")
            
            let packageDetailsObj = DMTPackageDetails()
            let featureBannerObj = DMTBannerClass()
            
            packageDetailsObj.featureItemId = packageJSON["id"].stringValue
            packageDetailsObj.featureItemTitle = packageJSON["title"].stringValue
            packageDetailsObj.featureItemCount = packageJSON["totalContentCount"].intValue
            var packagheItemDetailArray = Array<DMTPackageItemDetails>()
            for (_,contentDic) in packageJSON["content"] {
                
                let packagheItemDetailObj = DMTPackageItemDetails()
                packagheItemDetailObj.itemTitle = contentDic["title"].stringValue
                packagheItemDetailObj.itemDuration = contentDic["shortUrl"].intValue
                packagheItemDetailObj.itemId = contentDic["id"].stringValue
                
                var imageDic = contentDic["images"]
                
                if(imageDic["FEATURE_BANNER"].stringValue.count>0) {
                    packageDetailsObj.featureItemImageUrl.append(imageDic["FEATURE_BANNER"].stringValue)
                    featureBannerObj.itemTitle = contentDic["title"].stringValue
                    featureBannerObj.itemDuration = contentDic["shortUrl"].intValue
                    featureBannerObj.itemId = contentDic["id"].stringValue
                    featureBannerObj.itemRefTYpe = contentDic["refType"].stringValue
                    featureBannerObj.itemGenres = contentDic["genres"].stringValue
                    featureBannerObj.itemReleaseYear = contentDic["releaseYear"].stringValue
                    featureBannerObj.itemProgramType = contentDic["programType"].stringValue
                    for (_,languageContent) in contentDic["languages"] {
                        featureBannerObj.itemLanguageArray.append(languageContent.stringValue)
                    }
                    packageDetailsObj.featureBannerArray.append(featureBannerObj)
                }
                if(imageDic["PORTRAIT"].stringValue.count>0) {
                    packagheItemDetailObj.PotraitImageUrl = imageDic["PORTRAIT"].stringValue
                }
                if(imageDic["LANDSCAPE_43"].stringValue.count>0) {
                    packagheItemDetailObj.LandScapeImageUrl = imageDic["LANDSCAPE_43"].stringValue
                }
                packagheItemDetailObj.itemRefTYpe = contentDic["refType"].stringValue
                packagheItemDetailObj.itemGenres = contentDic["genres"].stringValue
                packagheItemDetailObj.itemReleaseYear = contentDic["releaseYear"].stringValue
                packagheItemDetailObj.itemProgramType = contentDic["programType"].stringValue
                
                for (_,languageContent) in contentDic["languages"] {
                    packagheItemDetailObj.itemLanguageArray.append(languageContent.stringValue)
                }
                packagheItemDetailArray.append(packagheItemDetailObj)
            }
            packageDetailsObj.itemDetailsArray = packagheItemDetailArray
            packageDetailsList.append(packageDetailsObj)
        }
        
        return packageDetailsList
    }

}
