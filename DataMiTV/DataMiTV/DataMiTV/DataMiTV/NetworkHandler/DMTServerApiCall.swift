//
//  DMTServerApiCall.swift
//  DataMiTV
//
//  Created by Sandeep Lall on 13/02/19.
//  Copyright © 2019 Sandeep Lall. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol DMTServerApiDelegate {
    func getResponseFromServer(swiftyJsonVar:JSON)
}

class DMTServerApiCall: NSObject {
    
    var delegate : DMTServerApiDelegate!

    func runRequestToServer(serverUrl:String) {
        
        Alamofire.request(serverUrl).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
               // print(responseData.error ?? "")
              //  print(responseData.result.value ?? "")
                self.delegate.getResponseFromServer(swiftyJsonVar: swiftyJsonVar)
            }
        }
    }
    
//    func getResponseFromServer(swiftyJsonVar:JSON) {
//        
//        print("swiftyJsonVar-------->\(swiftyJsonVar)")
//    }
}
