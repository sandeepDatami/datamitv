//
//  DMTPackageItemDetails.swift
//  DataMiTV
//
//  Created by Sandeep Lall on 18/02/19.
//  Copyright © 2019 Sandeep Lall. All rights reserved.
//

import UIKit

class DMTPackageItemDetails: NSObject {
    var itemId:String!
    var itemTitle:String!
    var itemImageUrl:String!
    var itemShortUrl:String!
    var itemDuration:Int!
    var LandScapeImageUrl:String!
    var featureBanner:String!
    var PotraitImageUrl:String!
    var itemLanguageArray = Array<String>()
    var itemRefTYpe:String!
    var itemGenres:String!
    var itemReleaseYear:String!
    var itemProgramType:String!
}
