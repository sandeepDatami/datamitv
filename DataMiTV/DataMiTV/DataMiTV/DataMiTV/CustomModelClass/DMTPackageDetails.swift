//
//  DMTPackageDetails.swift
//  DataMiTV
//
//  Created by Sandeep Lall on 15/02/19.
//  Copyright © 2019 Sandeep Lall. All rights reserved.
//

import UIKit

class DMTPackageDetails: NSObject {

    var featureItemId:String!
    var featureItemImageUrl = Array<String>()
    var featureItemTitle:String!
    var featureItemCount:Int!
    var itemDetailsArray = Array<DMTPackageItemDetails>()
    var featureBannerArray = Array<DMTBannerClass>()
}
