//
//  DMTCarouselCellTableViewCell.swift
//  DataMiTV
//
//  Created by Sandeep Lall on 11/02/19.
//  Copyright © 2019 Sandeep Lall. All rights reserved.
//

import UIKit

@objc protocol CarouselCellTableViewDelegate {
    func btnClickedForPlayingVideo(index:Int)
}

class DMTCarouselCellTableViewCell: UITableViewCell {

    @IBOutlet var caroselScrollView:UIScrollView!
    @IBOutlet var caroselImage:UIImageView!
    var delegate : CarouselCellTableViewDelegate?
    var imageWidth: CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setScrollViewScrollArea(itemCount:Int,itemWidth:Int) {
        
        caroselScrollView.contentSize = CGSize(width: CGFloat(itemCount * itemWidth), height: self.bounds.size.height)
        imageWidth = CGFloat(itemWidth)
    }
    
    func setImageToCaroselViewWithImageArray (imageArray:[String]) {
     
        var xAxis=0
        var btnCount = 0
        for imageName in imageArray {
            let imageBtn = UIButton()
            imageBtn.frame = CGRect(x: CGFloat(xAxis), y: 0.0, width: imageWidth, height: self.frame.size.height)
            imageBtn.setBackgroundImage(UIImage.init(named: imageName), for: .normal)
            imageBtn.isUserInteractionEnabled = true
            imageBtn.tag = btnCount
            btnCount = btnCount + 1
            imageBtn.addTarget(self, action: #selector(galleryImageButtonClicked(sender:)), for: .touchUpInside)
            caroselScrollView.addSubview(imageBtn)
            xAxis = xAxis + Int(imageBtn.frame.size.width) + 20
        }
    }
    
    @objc func galleryImageButtonClicked(sender:UIButton) {
        delegate?.btnClickedForPlayingVideo(index: sender.tag)
    }
    
}
