//
//  DMTVideoPlayerViewController.swift
//  DataMiTV
//
//  Created by Sandeep Lall on 12/02/19.
//  Copyright © 2019 Sandeep Lall. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit

class DMTVideoPlayerViewController: UIViewController {

    @IBOutlet var mediaPlayerView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showVideoPLayer()
        // Do any additional setup after loading the view.
    }
    
    func showVideoPLayer() {
//        let streamPlayer : MPMoviePlayerController =  MPMoviePlayerController(contentURL: NSURL(string:"http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8")! as URL)
//        // Do any additional setup after loading the view, typically from a nib.
//        streamPlayer.view.frame = self.view.bounds
////        mediaPlayerView?.addSubview(streamPlayer.view)
//        self.view.addSubview(streamPlayer.view)
//        streamPlayer.isFullscreen = true
//        // Play the movie!
//        streamPlayer.play()
        
        let videoURL = URL(string:"http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8")
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        present(playerViewController, animated: true) {
            player.play()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
