//
//  DMTHomePageViewController.swift
//  DataMiTV
//
//  Created by Sandeep Lall on 11/02/19.
//  Copyright © 2019 Sandeep Lall. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit
import SwiftyJSON

class DMTHomePageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CarouselCellTableViewDelegate,DMTServerApiDelegate {
    
    @IBOutlet var featuredListTable: UITableView?
    var imageArray = ["caroselImg1.jpg","caroselImg2.jpg","caroselImg3.jpg"]
    var serverObj = DMTServerApiCall()
    var parserObj = DMTParser()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationItem.setHidesBackButton(true, animated: false)
        featuredListTable?.delegate = self
        featuredListTable?.dataSource = self
        reigsterCell()
        let stringRepresentationOfId = CHANNEL_ID_ARRAY.joined(separator:",")  // "1-2-3"
        print(stringRepresentationOfId)
        serverObj.delegate = self
        serverObj.runRequestToServer(serverUrl: "https://content.airtel.tv/app/v1/package?id=\(stringRepresentationOfId)&isMax=false&appId=MOBILITY&mwTvPack=200292&dt=phone&os=ANDROID&ln=kn&bn=12566")
    }

    func reigsterCell() {
        featuredListTable?.register(UINib(nibName: "DMTCarouselCellTableViewCell", bundle: nil), forCellReuseIdentifier: "CarouselCell")
        featuredListTable?.register(UINib(nibName: "TripMapDataTableViewCell", bundle: nil), forCellReuseIdentifier: "TripMapDataTableViewCell")
        featuredListTable?.register(UINib(nibName: "TripInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "TripInfoTableViewCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: UItableviekw data Sourece and delegate method
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 200
        }
        else {
            return 150
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            return "Section 1"
        }
        else if section == 1 {
            return "Section 2"
        }
        else if section == 2 {
            return "Section 3"
        }
        else {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = (featuredListTable?.dequeueReusableCell(withIdentifier: "CarouselCell")) as! DMTCarouselCellTableViewCell
        
        print("indexPath.section------->\(indexPath.section)")
        
        if indexPath.section == 0 {
            cell.setScrollViewScrollArea(itemCount: imageArray.count, itemWidth: 300)
            cell.setImageToCaroselViewWithImageArray(imageArray: imageArray)
        }
        else {
            cell.setScrollViewScrollArea(itemCount: imageArray.count, itemWidth: 200)
            cell.setImageToCaroselViewWithImageArray(imageArray: imageArray)
        }
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
               
    }
    
    //MARK: CarouselCellTableViewDelegate Method called
    
    func btnClickedForPlayingVideo(index:Int) {
        
        let videoURL = URL(string:"https://wolverine.raywenderlich.com/content/ios/tutorials/video_streaming/foxVillage.m3u8")
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        present(playerViewController, animated: true) {
            player.play()
        }
    }
    
    func getResponseFromServer(swiftyJsonVar: JSON) {
        print("swiftyJsonVar-------->\(swiftyJsonVar)")
        let packageDetailList = parserObj.parseFeaturedPackageData(swiftyJsonVar: swiftyJsonVar)
        print(packageDetailList)
    }
}
